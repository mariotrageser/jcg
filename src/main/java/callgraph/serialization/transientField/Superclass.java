/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package callgraph.serialization.transientField;

import java.io.IOException;
import java.io.Serializable;

import org.opalj.annotations.callgraph.CallSite;
import org.opalj.annotations.callgraph.ResolvedMethod;
import org.opalj.annotations.callgraph.properties.EntryPoint;

/**
 * This class was used to create a class file with some well defined attributes. The
 * created class is subsequently used by several tests.
 * 
 * The nested class has a private method which is only called during de-serialization. However the 
 * call is gated by an check if the chooser field is not 0. This is never the case immediately after 
 * de-serialization as the field is transient and thus is de-serialized to 0.
 * 
 * <b>NOTE</b><br>
 * This class is not meant to be (automatically) recompiled; it just serves documentation
 * purposes.
 * 
 * <!--
 * 
 * 
 * INTENTIONALLY LEFT EMPTY TO MAKE SURE THAT THE SPECIFIED LINE NUMBERS ARE STABLE IF THE
 * CODE (E.G. IMPORTS) CHANGE.
 * 
 * 
 * -->
 * 
 * @author Roberts Kolosovs
 */
public class Superclass implements Serializable {

	private static final long serialVersionUID = 3005126240428648143L;
	
	ClassWithTransientField field = new ClassWithTransientField();
	
	private class ClassWithTransientField{ 
		transient int transientField; //transient field; value not saved during serialization
		
		@EntryPoint
		private void readObject(java.io.ObjectInputStream in) //entry point via de-serialization 
				throws IOException, ClassNotFoundException{
			in.defaultReadObject(); //transientField is restored to its default value (0 for int)
		}
		
		@CallSite(resolvedMethods = { 
				@ResolvedMethod(receiverType = "callgraph/serialization/transientField/Superclass$ClassWithTransientField") }, 
				name = "deadMethod", isStatic = false, line = 82)
		@EntryPoint
		private Object readResolve(){ //entry point via de-serialization, called after readObject
			if(transientField != 0){ //always false; transientField is always 0 after de-serialization
				deadMethod(); //dead code; branch never taken
			}
			return this; //default functionality;
		}
		
		private void deadMethod(){ //dead code; only call in dead branch of caller
			System.out.println("I feel dead inside.");
		}
	}

}
