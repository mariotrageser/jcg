package native_cmd;

import expressions.Constant;
import expressions.Expression;
import expressions.Map;
import expressions.Variable;
import expressions.jni.NativeAddExpression;
import expressions.jni.NativeDivExpression;
import expressions.jni.NativeMulExpression;
import expressions.jni.NativeSubExpression;

public class NativeExpressionEvaluator {
	public static void main(String[] args) {
		Map<String, Constant> values = new Map<String, Constant>();
		values.add("x", new Constant(1));
		Expression addExpression = new NativeAddExpression(new Variable("x"), new Constant(2));
		Expression subExpression = new NativeSubExpression(new NativeAddExpression(new Variable("x"), new Constant(2)),
				new Constant(3));
		Expression mulExpression = new NativeSubExpression(new NativeAddExpression(new Variable("x"), new Constant(2)),
				new NativeMulExpression(new Constant(3), new Constant(4)));
		Expression divExpression = new NativeDivExpression(
				new NativeSubExpression(new NativeAddExpression(new Variable("x"), new Constant(2)),
						new NativeMulExpression(new Constant(3), new Constant(4))),
				new Constant(3));

		System.out.println("Natively evaluating methods");
		System.out.println("values:");
		System.out.println("x->1");
		System.out.println();

		System.out.println("Natively evaluating expression ((x+2))");
		System.out.println("Result: " + addExpression.eval(values).getValue() + " (expected: 3)");
		System.out.println();

		System.out.println("Natively evaluating expression ((x+2)-3)");
		System.out.println("Result: " + subExpression.eval(values).getValue() + " (expected: 0)");
		System.out.println();

		System.out.println("Natively evaluating expression ((x+2)-(3*4))");
		System.out.println("Result: " + mulExpression.eval(values).getValue() + " (expected: -9)");
		System.out.println();

		System.out.println("Natively evaluating expression (((x+2)-(3*4))/3)");
		System.out.println("Result: " + divExpression.eval(values).getValue() + " (expected: -3)");
		System.out.println();
	}
}
