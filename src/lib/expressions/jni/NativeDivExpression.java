package expressions.jni;

import annotations.callgraph.CallSite;
import annotations.callgraph.ResolvedMethod;
import expressions.BinaryExpression;
import expressions.Constant;
import expressions.Expression;
import expressions.Map;
import expressions.Operator;

public class NativeDivExpression extends BinaryExpression {

	private static final long serialVersionUID = -6855720580775422348L;

	private final Object left;

	private final Object right;

	static {
		System.loadLibrary("arithmetic_operations");
	}

	public NativeDivExpression(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

	@Override
	@CallSite(name = "left", resolvedMethods = { @ResolvedMethod(receiverType = "expressions/AddExpression"),
			@ResolvedMethod(receiverType = "expressions/SubExpression"),
			@ResolvedMethod(receiverType = "fancy_expressions/MultExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/AddExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/SubExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/MulExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/DivExpression") })
	@CallSite(name = "right", resolvedMethods = { @ResolvedMethod(receiverType = "expressions/AddExpression"),
			@ResolvedMethod(receiverType = "expressions/SubExpression"),
			@ResolvedMethod(receiverType = "fancy_expressions/MultExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/AddExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/SubExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/MulExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/DivExpression") })
	@CallSite(name = "eval", resolvedMethods = { @ResolvedMethod(receiverType = "expressions/AddExpression"),
			@ResolvedMethod(receiverType = "expressions/SubExpression"),
			@ResolvedMethod(receiverType = "fancy_expressions/MultExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/AddExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/SubExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/MulExpression"),
			@ResolvedMethod(receiverType = "expressions/jni/DivExpression") })
	@CallSite(name = "getValue", resolvedMethods = { @ResolvedMethod(receiverType = "expressions/Constant") })
	@CallSite(name = "<init>", resolvedMethods = { @ResolvedMethod(receiverType = "expressions/Constant") })
	@CallSite(name = "<init>", resolvedMethods = { @ResolvedMethod(receiverType = "java/lang/NullPointerException") })
	@CallSite(name = "<init>", resolvedMethods = {
			@ResolvedMethod(receiverType = "java/lang/IllegalArgumentException") })
	public native Constant eval(Map<String, Constant> values);

	@Override
	protected Expression left() {
		return (Expression) left;
	}

	@Override
	protected Expression right() {
		return (Expression) right;
	}

	@Override
	protected Operator operator() {
		return NativeDivOperator.instance;
	}
}
