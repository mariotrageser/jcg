package expressions.jni;

import expressions.BinaryExpression;
import expressions.Expression;
import expressions.Operator;

public class NativeAddOperator extends Operator {
	
	private static final long serialVersionUID = -6010967472979013409L;

	public static final Operator instance = new NativeAddOperator();

	static BinaryExpression createBinaryExpression(Expression left, Expression right) {
		return new NativeAddExpression(left, right);
	}

	public String toString() {
		return "+[N]";
	}
}
