package expressions.jni;

import expressions.BinaryExpression;
import expressions.Expression;
import expressions.Operator;

public class NativeSubOperator extends Operator {
	
	private static final long serialVersionUID = 5303439860431688920L;

	public static final Operator instance = new NativeSubOperator();

	static BinaryExpression createBinaryExpression(Expression left, Expression right) {
		return new NativeSubExpression(left, right);
	}

	public String toString() {
		return "-[N]";
	}
}
