package expressions.jni;

import expressions.BinaryExpression;
import expressions.Expression;
import expressions.Operator;

public class NativeDivOperator extends Operator {
	
	private static final long serialVersionUID = 4453289275933952968L;
	
	public static final Operator instance = new NativeDivOperator();

	static BinaryExpression createBinaryExpression(Expression left, Expression right) {
		return new NativeDivExpression(left, right);
	}

	public String toString() {
		return "/[N]";
	}
}
