package expressions.jni;

import expressions.BinaryExpression;
import expressions.Expression;
import expressions.Operator;

public class NativeMulOperator extends Operator {

	private static final long serialVersionUID = -5761294318974412414L;

	public static final Operator instance = new NativeMulOperator();

	static BinaryExpression createBinaryExpression(Expression left, Expression right) {
		return new NativeMulExpression(left, right);
	}

	public String toString() {
		return "*[N]";
	}
}
