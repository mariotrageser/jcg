\documentclass{article}
\usepackage[utf8]{inputenc}

\title{OPAL-JCG Testsuite Documentation}
\author{Roberts Kolosovs}
\date{\today}

\begin{document}

\maketitle

\section{Introduction}\label{sec:introduction}

This document provides a structured overview of the test cases for java call graph algorithm testing located in the OPAL-JCG project. It is split into three sections. This section briefly describes the structure of this document and gives a guideline to extending and modifying it.\\
Section \ref{sec:oldtestsuite}, ''Old Test Suite'', contains the descriptions of the old test suite (the src/main/java folder) case by case grouped by the feature of the Java language they mainly test. The cases consist of a title, a list of instances in the old test suite implementing the case, a short description, and a list of cases in the new suite implementing this case. Some subsections of section \ref{sec:oldtestsuite} contain a ''Missing Cases'' part. That part contains brief descriptions of cases that possibly should be added to the test suite.\\
Section \ref{sec:newtestsuite}, ''New Test Suite'', contains the description of the new test suite (the src/app and src/lib folders) case by case grouped by the features of the Java language they mainly test. The cases consist of a title, a list of instances in the new test suite implementing this case, and a short description.\\
When implementing new test cases in the new test suite they should be documented here following the established pattern. The documentation of any old suite cases implementing this new case or case suggestions should be marked as being implemented in the new test suite.\\
If some of the test cases in the old or the new test suite are to be found invalid their description should not be removed. Instead they should be marked as invalid and a brief description of the problem should be added. This way the same mistake can be avoided in the future and the decision to invalidate a case can be reviewed.\\
New subsections can be added to the sections \ref{sec:oldtestsuite} and \ref{sec:newtestsuite} and test cases can be moved from one subsection to another. A test case should only be in one subsection even if it deals with multiple Java language features. Finally any changes to the structure of the documentation should be recorded in this section in order to keep this document consistent.


\section{Old Test Suite}\label{sec:oldtestsuite}
\subsection{Serialization}
\subsubsection{Implemented Cases}

\textbf{Simple Serializable Case}\\
Implemented in: 
\begin{itemize}
    \item serialization.simpleSerializable
\end{itemize}
Redundant implementation in: 
\begin{itemize}
    \item serialization.serializableAndExternalizable ((de-)serialization routine)
\end{itemize}
A class implements the Serializable interface. The methods writeReplace and readResolve are implemented with the default behavior (return this). The methods readObject and writeObject are implemented with the default behavior (inStream.defaultReadObject, outStream.defaultWriteObject). Another class implements methods containing standard implementation of serialization and de-serialization.\\

\noindent
\textbf{Simple Externalizable Case}\\
Implemented in: 
\begin{itemize}
    \item serialization.serializableAndExternalizable
\end{itemize}
A class implements the Externalizable interface. The methods writeReplace and readResolve are implemented with the default behavior (return this). The methods readExternal and writeExternal are implemented with the default behavior (do nothing). Another class implements methods containing standard implementation of externalization and de-externalization (identical to (de-)serialization).\\

\noindent
\textbf{Externalizable and Serializable Together}\\
Implemented in: 
\begin{itemize}
    \item serialization.serializableAndExternalizable
\end{itemize}
A class implements the Externalizable interface and the Serializable interface. The methods writeReplace and readResolve are implemented with the default behavior (return this). The methods readExternal and writeExternal are implemented with the default behavior (do nothing). The methods readObject and writeObject are implemented but never called as they are superceded by read/writeExternal during (de-)serialization.\\

\noindent
\textbf{Superclass with Private No-Args Constructor (Serializable)}\\
Implemented in: 
\begin{itemize}
    \item serialization.invalidSuperclass
\end{itemize}
A class implements the Serializable interface and extends another class. The superclass doesn't implement the Serializable interface and declares a private no-argument constructor. The subclass implements writeReplace, readResolve, readObject and writeObject with the default behavior. The de-serialization of the subclass always fails so readResolve and readObject methods of the subclass are never called.\\

\noindent
\textbf{Superclass with Private No-Args Constructor (Externalizable)}\\
Implemented in: 
\begin{itemize}
    \item serialization.extPrivateNoArgsConstructorSuperclass
\end{itemize}
A class implements the Externalizable interface and extends another class. The superclass doesn't implement the Serializable or Externalizable interfaces and declares a private no-argument constructor. In contrast to the serializable case the (de-)externalization of subclass completes without errors.\\

\noindent
\textbf{Serializable Superclass with Private No-Args Constructor}\\
Implemented in: 
\begin{itemize}
    \item serialization.serializableExtendingSerializable
\end{itemize}
A class implements the Serializable interface and extends another class. The superclass also implement the Serializable interface and declares a private no-argument constructor. In contrast to the serializable case with non-serializable superclass the (de-)serialization of the subclass completes without errors.\\

\noindent
\textbf{Call ReadObjectNoData}\\
Implemented in: 
\begin{itemize}
    \item serialization.serializableExtendingSerializable
\end{itemize}
A class implements the Serializable interface and extends another class implementing the Serializable interface. The superclass implements the readObjectNoData method with the default behavior (do nothing). ReadObjectNoData can be called during de-serialization of the subclass if the serialization happened before the inheritance was implemented.\\

\noindent
\textbf{Non-Serializable Field}\\
Implemented in: 
\begin{itemize}
    \item serialization.nonSerializableField
\end{itemize}
A class implements the Serializable interface and declares a field. The field's type is a class not implementing the Serializable interface. Both serialization and de-serialization of the serializable class fails.\\

\noindent
\textbf{Serializable Field}\\
Implemented in: 
\begin{itemize}
    \item serialization.serializableField
\end{itemize}
A class implements the Serializable interface and declares a field. The field's type is a class also implementing the Serializable interface. The writeReplace and writeObject methods of the field are called after the methods of the declaring class are called during serialization. During de-serialization readObject and readResolve methods of the field are called after readObject method but before readResolve method of the declaring class.\\

\noindent
\textbf{Transient Field}\\
Implemented in: 
\begin{itemize}
    \item serialization.transientField
\end{itemize}
A class implements the Serializable interface and declares a field marked as transient. The class also implements a private method which is called by the readResolve method during de-serialization if the transient field doesn't have its default value. However the transient field always has its type's default value at that point so the method is never called.\\

\noindent
\textbf{Invalid Constructor in Externalizable}\\
Implemented in: 
\begin{itemize}
    \item serialization.externalizableInvalidConstructor
\end{itemize}
A class implements the Externalizable interface and declares a private no-arguments constructor. The methods writeExternal and readExternal are declared. However the readExternal method is never called as every de-externalization attempt fails because the no-arguments constructor is nont public.\\

\noindent
\textbf{Field Values During De-Serialization}\\
Implemented in: 
\begin{itemize}
    \item serialization.codeDependsOnNonDefaultVal
\end{itemize}
A class implements the Serializable interface, declares a field and a method. The method is called during de-serialization by the readObject method if the field doesn't have its default value. However during de-serialization before inStream.defaultReadObject (or equivalent code) is executed each field has its types default value and thus the method is never called.\\

\noindent
\textbf{Visibility of ReadResolve and WriteReplace}\\
Implemented in: 
\begin{itemize}
    \item serialization.serializableExtendingSerializable
    \item serialization.publicReadResolveInSuperclass
    \item serialization.publicWriteReplaceInSuperclass
\end{itemize}
The readResolve and writeReplace methods of classes implementing the Serializable should be declared as private if implemented. If declared as public or protected, they are subject to usual visibility rules for class inheritance.\\

\noindent
\textbf{Visibility of ReadObject and WriteObject}\\
Implemented in: 
\begin{itemize}
    \item serialization.protectedReadWriteObject
\end{itemize}
The readObject and writeObject methods which can be implemented by serializable classes have to be private. They are not called during (de-)serialization if they are declared with some other visibility.\\

\noindent
\textbf{ReadResolve and WriteReplace Redirect Control Flow}\\
Implemented in: 
\begin{itemize}
    \item serialization.indirectReadResolve (new class created in another class by a static method)
    \item serialization.nestedClassNoNewInstances (de-serialized class and new class are both inner private classes of the same class)
\end{itemize}
The methods readResolve and writeReplace of classes implementing the Serializable interface are supposed to replace the (de-)serialized class with another class during (de-)serialization. This diverts the control flow to the newly returned class.\\

\noindent
\textbf{Interruption of the (De-)Serialization Process}\\
Implemented in: 
\begin{itemize}
    \item serialization.nestedExternalizable (error in readExternal)
\end{itemize}
During (de-)serialization the methods relevant to (de-)serialization are called in a specific order (if implemented). If an exception is thrown during one of the steps of (de-)serialization, none of the subsequent steps gets called.\\

\noindent
\textbf{Interface Inheritance}\\
Implemented in: 
\begin{itemize}
    \item serialization.nestedClassNoNewInstances
\end{itemize}
Each class implementing the Serializable interface or an interface extending Serializable has the same behavior in regard to (de-)serialization. A notable exception is the Externalizable interface (and its children), itself a child of the Serializable interface. Externalizable differs in behaviour from Serializable in some points and supersedes it.\\


\subsubsection{Missing Cases}

\textbf{Empty ReadObject}\\
A serializable class implements the readObject method with empty body (do-nothing behavior). All class fields are set to their default values after readObject is called during de-serialization. This can influence method calls in later steps of de-serialization if they depend on specific field values.\\

\noindent
\textbf{Empty WriteObject}\\
A serializable class implements the writeObject method with empty body (do-nothing behavior). Nothing is written to stream during serialization. When de-serializing a stream written this way the inStream.defaultReadObject throws a StreamCorruptedException. This can influence method calls in later steps of de-serialization. However this behavior is only exhibited when de-serializing the current version of the code. If another versions of the class were serialized, their de-serialization doesn't necessarily always produce an error.\\


\subsection{Field Access}
\subsubsection{Implemented Cases}

\textbf{Primitive Field Access}\\
Implemented in: 
\begin{itemize}
    \item accessFields
\end{itemize}
A class declares a field of type T and a method accessing a public field of the class T. The type of the accessed field is a Java primitive.\\

\noindent
\textbf{Object Field Access}\\
Implemented in: 
\begin{itemize}
    \item accessFields
\end{itemize}
A class declares a field of type T and a method accessing a public field of the class T. The type of the accessed field is a child of the Java Object class.\\

\noindent
\textbf{Shadowed Superclass Field Access}\\
Implemented in: 
\begin{itemize}
    \item accessFields
\end{itemize}
A class declares a field of type T and a method accessing a public field of the class T. The accessed field shares his type and name with a field in the direct superclass of T shadowing it. The field's type is a child of the Java Object class.\\

\noindent
\textbf{Static Field Access}\\
Implemented in: 
\begin{itemize}
    \item accessFields
\end{itemize}
A class declares a method accessing a static field of another class. The accessed fields type is a Java primitive.\\

\subsubsection{Missing Cases}

\textbf{Shadowed Static Field Access}\\
A class declares a method accessing a static field of another class C. The accessed field shares his type and name with a field in the direct superclass of C shadowing it.\\

\noindent
\textbf{Shadowed Primitive Field}\\
The same test case as the ''Shadowed Superclass Field Access'' case but with accessed field's type being a Java primitive type.\\

\noindent
\textbf{Static Non-Primitive Field}\\
The same test cases as the ''Static Field Access'' and ''Shadowed Static Field Access'' but with the accessed field's type being a child of the Java Object class.\\


\subsection{Branch Elimination Based On Dynamic Typing}

\textbf{Call a Method of a Field with Value Set in Constructor}\\
Implemented in: 
\begin{itemize}
    \item branchElimination
\end{itemize}
A class C defines a field statically typed as C and a method calling a method of the field. The field's value is set in the constructor of C and never changed afterwards. The field's dynamic type is a second generation subclass of the class C.\\

\noindent
\textbf{Call a Method of a Field Fetched via a Method Call}\\
Implemented in: 
\begin{itemize}
    \item branchElimination
\end{itemize}
The same case as ''Call a Method of a Field with Value Set in Constructor'' with the exception that the field is fetched via a method call instead of direct access.\\

\noindent
\textbf{Call a Method of a Field Statically Typed as a Mid-Level Class}\\
Implemented in: 
\begin{itemize}
    \item branchElimination
\end{itemize}
The same case as ''Call a Method of a Field with Value Set in Constructor'' with the exception that the field is statically typed to a direct subclass of C instead of C itself. The dynamic type is a direct subclass of the static type.\\

\noindent
\textbf{Call a Method of a Variable with Value Set in an If-Clause}\\
Implemented in: 
\begin{itemize}
    \item branchElimination
\end{itemize}
A class C defines a method defining a variable and invoking a method of the variable. The variable is statically typed as the class C. Its final value (and thus dynamic type) is set in an if-clause and depends on the dynamic type of a field of the class C. The field is statically typed as the class C, set to its value in the constructor and fetched via a method call.\\

\noindent
\textbf{Call a Method of a Field Dynamically Typed as an Inner Class}\\
Implemented in: 
\begin{itemize}
    \item highPrecision
\end{itemize}
Redundant implementation in: 
\begin{itemize}
    \item simpleCallgraph (regular class instead of an inner class)
\end{itemize}
A class C defines a method calling a method of a field. The field is statically typed as an interface and dynamically typed as an inner class of the class C. The field's value is set as the default value of the field and not changed afterwards.\\

\noindent
\textbf{Call a Method of a Variable Dynamically Typed as an Anonymous Class}\\
Implemented in: 
\begin{itemize}
    \item highPrecision
\end{itemize}
A class defines a method defining a variable and calling a method of that variable. The variable is statically typed as an interface. Its dynamic type is an anonymous class. The interface has a subtype which is an inner class.\\


\subsection{Constructor Calls}
\subsubsection{Implemented Cases}

\textbf{Simple Constructor Call}\\
Implemented in: 
\begin{itemize}
    \item constructors
\end{itemize}
A class defines a method declaring a variable and initializing it by calling a no-arguments constructor.\\

\noindent
\textbf{Parameterized Constructor Call}\\
Implemented in: 
\begin{itemize}
    \item constructors
\end{itemize}
A class defines a method declaring a variable and initializing it by calling a unary constructor.\\

\noindent
\textbf{Inner Class Constructor Call}\\
Implemented in: 
\begin{itemize}
    \item constructors
\end{itemize}
Same as ''Simple Constructor Call'' but with the constructor being called belonging to an inner class.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling Constructor of Inner Class
\end{itemize}

\noindent
\textbf{Call Constructor Without Assignment}\\
Implemented in: 
\begin{itemize}
    \item simpleCallgraph
\end{itemize}
Redundant implementation in: 
\begin{itemize}
    \item reflections (additional cast to implemented interface)
\end{itemize}
A class defines a method calling a constructor and immediately calling a method of the newly created object. The object itself is then discarded instead of being assigned to a field or a variable.\\

\noindent
\textbf{Indirect Constructor Call via Reflection}\\
Implemented in: 
\begin{itemize}
    \item reflections
\end{itemize}
A class defines a method calling a constructor indirectly via the newInstance method of of the Class class.\\
Implementation in new test suite: 
\begin{itemize}
    \item Class Instantiation via Reflection
\end{itemize}

\subsubsection{Missing Cases}

\textbf{Instantiating a Generic Class}\\
A call to the constructor of a generic class.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling Constructor of Generic Class
\end{itemize}

\subsection{Try-Catch and Exception Handling}
\subsubsection{Implemented Cases}

\textbf{Simple Call in Catch}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callVirtualMethodInCatch
\end{itemize}
A class defines a method containing a try-catch block. The try-block throws an exception which is caught in the catch-block. The catch-statement consists of a simple method call.\\

\noindent
\textbf{Static Call in Catch}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callStaticMethodInCatch
\end{itemize}
Same as ''Simple Call in Catch'' but the method called in the catch-block is a static method.\\

\noindent
\textbf{Catch Block Never Executed}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:notCallVirtualMethodInCatch
\end{itemize}
Same as ''Simple Call in Catch'' but the try-block consists of a statement which doesn't throw any exceptions. The catch-block is never executed.\\

\noindent
\textbf{Catch Block with Static Method Never Executed}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:notCallStaticMethodInCatch
\end{itemize}
Same as ''Catch Block Never Executed'' but the method call in the catch-block is a static call. It is still never executed.\\

\noindent
\textbf{Simple Call in Finally}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callVirtualMethodInFinally
\end{itemize}
A class defines a method containing a try-catch-finally block. The try-block throws an exception which is caught in the catch-block (otherwise empty). The finally-block then performs a simple method call.\\

\noindent
\textbf{Static Call in Finally}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callStaticMethodInFinally
\end{itemize}
Same as ''Simple Call in Finally'' except the finally-block consists of a static call.\\

\noindent
\textbf{Try a Method Throwing Exception}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callsMethodThatThrowsException
\end{itemize}
Same as the ''Static Call in Catch'' except the try-block consists of a method call. The called method always throws an exception.\\

\noindent
\textbf{Several Catch-Statements, Only One Executed}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:throwErrorCatchExceptionOrError
\end{itemize}
The try-block throws a specific kind of error or exception. The try-block is followed by two catch-blocks both of which contain a method call. The first catch-block catches the wrong throwable and is never executed. The second catches the right throwable and is executed.\\

\noindent
\textbf{Several Catch-Statements, Every Can Be Executed}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callMethodBasedOnThrowableType
\end{itemize}
The try-block may throws several different kinds of exceptions based on run-time behavior (here: hashing the code of the object). It is followed by several catch-blocks, one for every kind of throwable, each calling a different method. All of them are potentially executed.\\

\noindent
\textbf{Several Catch-Statements, All But One Can Be Executed}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callMethodBasedOnThrowableTypePartly
\end{itemize}
Same as ''Several Catch-Statements, Every Can Be Executed'' except there is at least one catch-statement catching an exception never thrown in the try-block (and thus that catch is never executed).\\

\noindent
\textbf{Possible Exception Throw Followed by Guaranteed Exception Throw in Try}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callMultipleMethodsInTry
\end{itemize}
The try-block calls two methods the first sometimes throwing an exception the second always throwing a different exception. Both exceptions are caught in separate catch-blocks and result in calls of two different methods. Botch method calls are possible.\\

\noindent
\textbf{Guaranteed Exception Throw Followed by Possible Exception Throw in Try}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callMultipleMethodsInTryAlternateOrder
\end{itemize}
Same as ''Possible Exception Throw Followed by Guaranteed Exception Throw in Try'' except the method calls in try are in reverse order. The one always throwing an exception is called first and thus the catch-block catching the possible exception is never executed.\\

\subsubsection{Missing Cases}

\textbf{Finally Statement with Try Never Throwing Exceptions}\\
If a try-catch-finally is declared the finally statement is always executed (bar the JVM exiting or the thread being killed while the try-block is being executed). There should be a test case (or two: regular and static calls) for a method call in a finally-block with the try-block never throwing an exception.\\

\noindent
\textbf{Method Declared to Throw but Never Throwing in Try}\\
The try-block calls a method declared to throw an exception. However the part of the method throwing the exception is dead code.\\

\subsection{Loops}

\textbf{Call in For-Loop After Break}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callInCatchWithBreak
\end{itemize}
A method contains a for-loop with a break statement followed by a static method call. The static call is never executed as the break statement terminates the loop.\\

\noindent
\textbf{Call in For-Loop After Labeled Break}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:noCallInCatchWithLabeledBreak
\end{itemize}
A method contains a labeled for-loop with another for-loop nested inside of it. The inner loop contains a break statement breaking the outer loop via the label and a method call afterwards. The method call is never executed.\\

\noindent
\textbf{Call in Empty For-Loop}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callInEmptyForLoop
\end{itemize}
A method contains a for-loop with zero iterations with a method call inside of it. The method call is never executed.\\

\noindent
\textbf{Infinite Loop Before Method Call}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:noCallInMethodThatThrowsExceptionCatchInfiniteLoop
\end{itemize}
A method contains an infinite while-loop followed by a method call. The method call is never executed.\\

\noindent
\textbf{Non-Infinite Loop Before Method Call}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callsMethodThatThrowsExceptionCatchNoInfiniteLoop
\end{itemize}
A method contains a while-loop with zero iterations followed by a method call. The method call gets ecxecuted as normal.\\

\noindent
\textbf{Method Call in a Do-While Loop with Always False Guard}\\
Implemented in: 
\begin{itemize}
    \item misc.TryCatchFinally:callMethodInCatchWithDoWhile
\end{itemize}
A method contains a do-while-loop with a trivially false guard. The loop contains a method call which is executed once during the first and only loop iteration.\\

    
\subsection{Class Instantiability}

\textbf{Class with Private Constructor and a Factory Method}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.FactoryAndInstantiable
\end{itemize}
A public class defining only private constructors and a package private factory method.\\

\noindent
\textbf{Class with Private Constructor and a Native Factory Method}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.NativeFactoryMethod
\end{itemize}
A public class defining only private constructors and a public native factory method.\\

\noindent
\textbf{Class with Protected Constructor and a Fake Factory Method}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.NoFactoryButInstantiable
\end{itemize}
A public class defining a private and a protected constructor. It also defines a method resembling a factory method but always returning null.\\

\noindent
\textbf{Class with Only Private Constructors}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.OnlyPrivateConstructors
\end{itemize}
A public class defining only private constructors and no factory methods. Can't be instantiated.\\

\noindent
\textbf{Abstract Class without Explicit Constructors}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.PackagePrivateAbstractClass
\end{itemize}
A package private abstract class without explicit constructor definitions. Can't be instantiated.\\

\noindent
\textbf{Class with Protected Constructor and Private Factory Method}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.PackageVisibleFactoryMethod, properties.instantiability.TransFacoryMethod
\end{itemize}
A class defines only protected constructors and a private factory method. Another class in the package defines a protected factory method for the class.\\

\noindent
\textbf{Class with a Real and a Fake Factory Method}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.PublicFactoryMethod
\end{itemize}
A class defines only private constructors and a public factory method. It also defines another method similar to a factory method but with void return value.\\

\noindent
\textbf{Public Interface}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.PublicInterface
\end{itemize}
An empty public interface. Can't be instantiated.\\

\noindent
\textbf{Public Serializable Class}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.SerializableClass
\end{itemize}
An empty public class implementing the Serializable interface. The Serializable interface doesn't prevent the class from being instantiated.\\

\noindent
\textbf{Class with Private Constructor and a Factory Method for other Class}\\
Implemented in: 
\begin{itemize}
    \item properties.instantiability.TransFacoryMethod
\end{itemize}
A class defines only private constructors and a protected factory method. However the factory method builds another class in the package.\\

\subsection{Reflection}

\textbf{Calling a Method of a Class Instantiated via Reflection}\\
Implemented in: 
\begin{itemize}
    \item reflections
\end{itemize}
A class defines a method instantiating a class via it's Class object's newInstance method. It then calls a method of the instantiated class.\\

\noindent
\textbf{Invoking a Method via Reflection}\\
Implemented in: 
\begin{itemize}
    \item reflections
\end{itemize}
A class defines a method instantiating a class and fetching the Method object of a method of the instantiated class. It then invokes the method via the invoke method of the Method class.\\
Implementation in new test suite: 
\begin{itemize}
    \item Invoking a Method via Reflection
\end{itemize}


\subsection{Static Calls}

\subsubsection{Implemented Cases}

\textbf{Call a Static Method of a Concrete Class}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.CallStaticMethods
\end{itemize}
A class declares a method calling a static method of another concrete class.\\

\noindent
\textbf{Call a Static Method of an Abstract Class}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.CallStaticMethods
\end{itemize}
A class declares a method calling a static method of an abstract class. The called method is concrete.\\

\noindent
\textbf{Static Initializer Calling Methods of a Concrete Class}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.StaticInitializerConcreteObjects
\end{itemize}
A class defines a static initializer instantiating a concrete class and calls a method of that class.\\

\noindent
\textbf{Static Initializer Calling Methods of an Abstract Class}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.StaticInitializerConcreteObjects
\end{itemize}
A class defines a static initializer instantiating an abstract class and calls a concrete method of that class.\\

\noindent
\textbf{Static Initializer Calling Methods of a Concrete Class Typed as Interface}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.StaticInitializerInterfaceObjects
\end{itemize}
A class defines a static initializer instantiating a concrete class statically typed as an interface and calls a method of that class.\\

\noindent
\textbf{Static Initializer Calling Methods of an Abstract Class Typed as Interface}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.StaticInitializerInterfaceObjects
\end{itemize}
A class defines a static initializer instantiating an abstract class statically typed as an interface and calls a method of that class.\\

\noindent
\textbf{Static Initializer Calling Static Methods of a Concrete Class}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.StaticInitializerStaticMethods
\end{itemize}
A class defines a static initializer calling a static method of a concrete class.\\

\noindent
\textbf{Static Initializer Calling Static Methods of an Abstract Class}\\
Implemented in: 
\begin{itemize}
    \item staticCalls.StaticInitializerStaticMethods
\end{itemize}
A class defines a static initializer calling a concrete static method of an abstract class.\\

\subsubsection{Missing Cases}

\textbf{Static Calls in Argument Position of Another Method Call}\\
A method taking arguments is called and a static method is called in an argument position during the first call.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling a Static Method as a Parameter During Method Call
\end{itemize}


\subsection{Virtual Calls}
\subsubsection{Implemented Cases}

\textbf{Calling Method Defined in Abstract Superclass}\\
Implemented in: 
\begin{itemize}
    \item base.AlternateBase
\end{itemize}
A concrete class extending an abstract class declares a method calling another method of the class. The called method is defined in the abstract superclass and declared in an interface implemented by the abstract superclass.\\

\noindent
\textbf{Call Own Private Method}\\
Implemented in: 
\begin{itemize}
    \item serialization.publicReadResolveInSuperclass
\end{itemize}
A class declares two methods. One of them is private and is called by the other method, which is an entry point.\\

\noindent
\textbf{Calling the ToString or HashCode Methods of a Concrete Class}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallToStringOnInterface
    \item virtualCalls.CallHashcodeOnInterface
\end{itemize}
A class calls the toString or hashCode method of an object statically typed as an interface and dynamically typed as a concrete class implementing the interface.\\

\noindent
\textbf{Calling the ToString or HashCode Methods of an Abstract Class}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallToStringOnInterface
    \item virtualCalls.CallHashcodeOnInterface
\end{itemize}
A class calls the toString or hashCode method of an object statically typed as an interface and dynamically typed as an abstract class implementing the interface.\\

\noindent
\textbf{Call a Method of a Concrete Argument}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallByParameter
\end{itemize}
A class declares a method which takes an argument statically typed as a concrete class. The method calls a method of the class passed as argument.\\

\noindent
\textbf{Call a Method of an Abstract Argument}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallByParameter
\end{itemize}
A class declares a method which takes an argument statically typed as an abstract class. The method calls a concrete method of the class passed as argument.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling Method of Abstract Generic Parameter
\end{itemize}

\noindent
\textbf{Call a Method of an Interface Argument}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallByParameter
\end{itemize}
A class declares a method which takes an argument statically typed as an interface. The method calls a method of the interface passed as argument.\\

\noindent
\textbf{Call a Method of a Concrete Field}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallConcreteObjects
\end{itemize}
A class declares a method and a field statically typed as a concrete class. The method calls a method of the field.\\

\noindent
\textbf{Call a Method of an Abstract Field}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallConcreteObjects
\end{itemize}
A class declares a method and a field statically typed as an abstract class. The method calls a method of the field.\\

\noindent
\textbf{Call a Method of an Interface Field}\\
Implemented in: 
\begin{itemize}
    \item virtualCalls.CallInterfaceObjects
\end{itemize}
A class declares a method and a field statically typed as an interface. The method calls a method of the field.\\

\noindent
\textbf{Call a Super Method}\\
Implemented in: 
\begin{itemize}
    \item base.AlternateBase
\end{itemize}
A class extends an abstract class and overrides a method implemented by the superclass. The overridden method calls the superclass version of itself via the 'super' keyword.\\

\subsubsection{Missing Cases}

\textbf{Methods of Generic Classes}\\
Calls to methods defined by generically typed classes. The class may be concrete of abstract. The methods may be static or not and have generic parameters and/or return values or not.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling Method of Abstract Generic Parameter
\end{itemize}

\noindent
\textbf{Method Call in Argument Position of Another Method Call}\\
A method taking arguments is called and another method is called in an argument position of the first method call.\\

\noindent
\textbf{Calls to Methods of Arrays}\\
Calls to methods of arrays of Java primitive types and non-primitive types.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling Method of an Array Parameter (non-primitive type)
\end{itemize}

\noindent
\textbf{Calls to Methods of Inner Classes}\\
Calls to methods of inner classes outside of those classes.\\
Implementation in new test suite: 
\begin{itemize}
    \item Calling a Method of an Inner Class
\end{itemize}


\section{New Test Suite}\label{sec:newtestsuite}

\subsection{Reflections}

\textbf{Class Instantiation via Reflection}\\
Implemented in: 
\begin{itemize}
    \item expressions.BinaryExpression
\end{itemize}
A method declares a field and calls the newInstance method of the Class class to construct the instance to initialize the field with.\\

\noindent
\textbf{Invoking a Method via Reflection}\\
Implemented in: 
\begin{itemize}
    \item expressions.BinaryExpression
\end{itemize}
A method is called by creating a corresponding Method object and calling its invoke method.\\

\subsection{Virtual Calls}

\textbf{Calling Method of Abstract Generic Parameter}\\
Implemented in: 
\begin{itemize}
    \item expressions.Constant
\end{itemize}
A method excepts an input and calls a method of the input object. The input is statically typed as an abstract, generic class.\\

\noindent
\textbf{Calling Method of an Array Parameter}\\
Implemented in: 
\begin{itemize}
    \item cmd.ExpressionEvaluator
\end{itemize}
A method excepts an input and calls a method of the input object. The input is statically typed as an array of non-primitive types.\\

\noindent
\textbf{Calling Method of Concrete Generic Variable}\\
Implemented in: 
\begin{itemize}
    \item cmd.ExpressionEvaluator
\end{itemize}
A method defines a variable statically typed as a generic type. Then a method of the variable is called.\\

\noindent
\textbf{Calling a Method of an Inner Class}\\
Implemented in: 
\begin{itemize}
    \item expressions.Map
\end{itemize}
A class C defines an inner class with some methods. Class C also defines a method which calls a method of the inner class.\\

\subsection{Static Calls}

\textbf{Calling a Static Method as a Parameter During Method Call}\\
Implemented in: 
\begin{itemize}
    \item cmd.ExpressionEvaluator
\end{itemize}
A method taking an arguments is called. Another method call to a static method is called in an argument position of the first method.\\

\subsection{Constructor Calls}

\textbf{Calling Constructor of Inner Class}\\
Implemented in: 
\begin{itemize}
    \item expressions.Map
\end{itemize}
A class defines an inner class and a method declaring a variable and instantiating it with an instance of the inner class.\\

\noindent
\textbf{Calling Constructor of Generic Class}\\
Implemented in: 
\begin{itemize}
    \item cmd.ExpressionEvaluator
\end{itemize}
A class defines a method declaring a field and instantiating it with an instance of a generic class.\\


\subsection{Callback}

\textbf{Utilizing Callback}\\
Implemented in: 
\begin{itemize}
    \item cmd.ExpressionEvaluator
\end{itemize}
A class utilizes the Callback feature of Java.\\


\end{document}
